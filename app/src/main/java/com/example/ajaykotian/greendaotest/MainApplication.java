package com.example.ajaykotian.greendaotest;

import android.app.Application;

import org.greenrobot.greendao.database.Database;

/**
 * Created by ajaykotian on 14/7/17.
 */

public class MainApplication extends Application {

    /** A flag to show how easily you can switch from standard SQLite to the encrypted SQLCipher. */
    public static final boolean ENCRYPTED = false;

    private DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();


        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "test-db");
        Database db = helper.getWritableDb();

        //for Encrypted;
       /* helper = new DaoMaster.DevOpenHelper(this, "test-db-encrypted");
        db = helper.getEncryptedWritableDb("super-secret");*/

        daoSession = new DaoMaster(db).newSession();
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }
}
