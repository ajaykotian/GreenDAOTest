package com.example.ajaykotian.greendaotest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.WhereCondition;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    protected EditText editText;
    protected Button submit;
    protected Button delete;
    protected TextView textView;
    protected TableDao tableDao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = (EditText) findViewById(R.id.et_main);
        textView = (TextView) findViewById(R.id.tv_result);
        submit = (Button) findViewById(R.id.button_submit);
        delete = (Button) findViewById(R.id.button_delete);

        DaoSession daoSession = ((MainApplication)this.getApplication()).getDaoSession();
        tableDao = daoSession.getTableDao();



        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String text = editText.getText().toString();

                if(!text.isEmpty())
                {
                    Table table = new Table();
                    table.setVal(Integer.parseInt(text));

                    long result = tableDao.insert(table);

                    if(result>=0)
                    {
                        showData();
                    }
                }
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List table = tableDao.queryBuilder().orderDesc(TableDao.Properties.Id).list();

                if(!table.isEmpty()) {
                    Table element = (Table) table.get(0);

                    tableDao.delete(element);
                }
                showData();
            }
        });

    }

    protected void showData()
    {
        List tabledata = tableDao.queryBuilder().list();

        String output="";

        for(int i=0; i<tabledata.size();i++)
        {
            Table temp = (Table) tabledata.get(i);
            output+="Id:  "+temp.getId()+"  Val:  "+temp.getVal()+"\n";
        }

        textView.setText(output);
    }
}
