package com.example.ajaykotian.greendaotest;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by ajaykotian on 14/7/17.
 */
@Entity
public class Table {

    @Id(autoincrement = true)
    private Long id;

    @NotNull
    private int val;

    @Generated(hash = 1230487986)
    public Table(Long id, int val) {
        this.id = id;
        this.val = val;
    }

    @Generated(hash = 752389689)
    public Table() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }
}
